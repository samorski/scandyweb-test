<?php
   
    require 'php/classes/DbConnect.php';

    $connection = new DbConnect();
   
  
?>


<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Add Product</title>
</head>

<body>
    <div class="container-fluid p-3">
        <div class="row">
            <div class="col">
                <form id="product_form" action="php/SaveProduct.php" method="POST">
                    <div class="row mt-5">
                        <div class="col-md-6">
                            <h1>Product Add</h1>
                        </div>
                        <div class="col-md-6">
                            <div class="float-right mr-3">
                                <button type="submit"
                                    class="btn btn-outline-dark btn-shadow mr-2 mr-5">Save</button>
                                <a class="btn btn-outline-dark btn-shadow" href="index.php">Cancel</a>
                            </div>
                        </div>

                    </div>
                    <div class="border"></div>
                    <div class="form-group row mt-5">
                        <label for="sku" class="col-sm-1 offset-sm-1 col-form-label">SKU</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="sku" name="sku">
                            <small id="skuDesc" class="form-text">
                                Please add the SKU (stok-keeping unit) for the product, this should be unique
                                combination of alphanumeric characters.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="name" class="col-sm-1 offset-sm-1 col-form-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name">
                            <small id="nameDesc" class="form-text">
                                Please add the product name.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="price" class="col-sm-1 offset-sm-1 col-form-label">Price ($)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="price" name="price">
                            <small id="priceDesc" class="form-text">
                                Please add the product price.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="productType" class="col-sm-1 offset-sm-1 col-form-label">Type Switcher</label>
                        <div class="col-sm-8">
                            <select id="productType" class="custom-select" name="productType">
                                <option selected disabled value="">Type Switcher</option>
                                <?php
                                    $sql= "SELECT * FROM product_type";
                                    $query =  $connection->connect()->query($sql);
                                    while($result = $query->fetch()){
                                        echo "<option value='{$result["name"]}'>{$result["name"]}</option>";
                                    };
                                ?>
                            </select>
                            <small id="productTypeDesc" class="form-text">
                                Please add the product type.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="size" class="col-sm-1 offset-sm-1 col-form-label DVD">Size (MB)</label>
                        <div class="col-sm-8 DVDi">
                            <input type="text" class="form-control" id="size" name="size">
                            <small id="sizeDesc" class="form-text">
                                Please add the product size.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row  ">
                        <label for="weight" class="col-sm-1 offset-sm-1 col-form-label book">Weight (KG)</label>
                        <div class="col-sm-8 booki">
                            <input type="text" class="form-control" id="weight" name="weight">
                            <small id="weightDesc" class="form-text">
                                Please add the product weight.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row  ">
                        <label for="height" class="col-sm-1 offset-sm-1 col-form-label height">Height (CM)</label>
                        <div class="col-sm-8 heighti">
                            <input type="text" class="form-control" id="height" name="height">
                            <small id="heightDesc" class="form-text">
                                please add the product height.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row  ">
                        <label for="width" class="col-sm-1 offset-sm-1 col-form-label width">Width (CM)</label>
                        <div class="col-sm-8 widthi">
                            <input type="text" class="form-control" id="width" name="width">
                            <small id="widthDesc" class="form-text">
                                Please add the product width.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row  ">
                        <label for="length" class="col-sm-1 offset-sm-1 col-form-label length">Length (CM)</label>
                        <div class="col-sm-8 lengthi">
                            <input type="text" class="form-control" id="length" name="length">
                            <small id="lengthDesc" class="form-text">
                                Please add the product length.
                            </small>
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>


    <script src="js/jquery-3.6.0.js"></script>
    <script src="js/addproduct.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>