<?php 

class InsertProduct extends Product{

    public $size;
    public $weight;
    public $height;
    public $length;
    public $width;
    
    public function __construct($sku,$name,$price,$productType,$size,$height,$weight,$length,$width)
    {
       
        parent::__construct($sku,$name,$price,$productType);
        $this->size = $size;
        $this->height = $height;
        $this->weight = $weight;
        $this->length = $length;
        $this->width = $width;

        
    }



    public function setProduct(){
         
        
        $sql = "INSERT INTO products (SKU,name,price,product_type,size,weight,length,width,height)
        VALUES (:SKU,:name,:price,:product_type,:size,:weight,:length,:width,:height)";
           
        $query = $this->connect()->prepare($sql);
        $result = $query->execute([

            'SKU' => $this->sku,
            'name' => $this->name,
            'price' => $this->price,
            'product_type' => $this->productType,
            'size' => $this->size,
            'weight' => $this->weight,
            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height
        ]);
          
        header('Location: ../index.php');
      
    }
    
    public function validateProduct(){
        
        
        if ($this->productType == 'DVD') {
            $errSKU = '';
            $errName = '';
            $errPrice = '';
            $errProductType = '';
            $errSize = '';

            $success=false;
            $hasError = false;

            if (!isset($this->sku) || empty($this->sku)) {
                $errSKU = '* SKU field is required';
                $hasError = true;
                return $errSKU;
            }
            if (!isset($this->name) || empty($this->name)) {
                $errName = '* name field is required';
                $hasError = true;
                return $errName;
            }
            if (!isset($this->price) || empty($this->price)) {
                $errPrice = '* Price field is required';
                $hasError = true;
                return $errPrice;
            }
            if (!isset($this->productType) || empty($this->productType)) {
                $errProductType = '* Product type field is required';
                $hasError = true;
                return $errProductType;
            }
            if (!isset($this->size) || empty($this->size)) {
                $errSize = '* Size field is required';
                $hasError = true;
                return $errSize;
            }
            if (!$hasError) {
               $success = true;
            }
        }else{
            
        }

        if ($this->productType == 'Book') {
            $errSKU = '';
            $errName = '';
            $errPrice = '';
            $errProductType = '';
            $errWeight = '';

            $success=false;
            $hasError = false;

            if (!isset($this->sku) || empty($this->sku)) {
                $errSKU = '* SKU field is required';
                $hasError = true;
                return $errSKU;
            }
            if (!isset($this->name) || empty($this->name)) {
                $errName = '* name field is required';
                $hasError = true;
                return $errName;
            }
            if (!isset($this->price) || empty($this->price)) {
                $errPrice = '* Price field is required';
                $hasError = true;
                return $errPrice;
            }
            if (!isset($this->productType) || empty($this->productType)) {
                $errProductType = '* Product type field is required';
                $hasError = true;
                return $errProductType;
            }
            if (!isset($this->weight) || empty($this->weight)) {
                $errWeight = '* Size field is required';
                $hasError = true;
                return $errWeight;
            }
            if (!$hasError) {
               $success = true;
            }
        }else{
            
        }

        if ($this->productType == 'Furniture') {
            $errSKU = '';
            $errName = '';
            $errPrice = '';
            $errProductType = '';
            $errHeight = '';
            $errLength = '';
            $errWidth = '';
            $success=false;
            $hasError = false;

            if (!isset($this->sku) || empty($this->sku)) {
                $errSKU = '* SKU field is required';
                $hasError = true;
                return $errSKU;
            }
            if (!isset($this->name) || empty($this->name)) {
                $errName = '* name field is required';
                $hasError = true;
                return $errName;
            }
            if (!isset($this->price) || empty($this->price)) {
                $errPrice = '* Price field is required';
                $hasError = true;
                return $errPrice;
            }
            if (!isset($this->productType) || empty($this->productType)) {
                $errProductType = '* Product type field is required';
                $hasError = true;
                return $errProductType;
            }
            if (!isset($this->height) || empty($this->height)) {
                $errHeight = '* Size field is required';
                $hasError = true;
                return $errHeight;
            }
            if (!isset($this->width) || empty($this->width)) {
                $errWidth = '* Size field is required';
                $hasError = true;
                return $errWidth;
            }
            if (!isset($this->length) || empty($this->length)) {
                $errLength = '* Size field is required';
                $hasError = true;
                return $errLength;
            }
            if (!$hasError) {
               $success = true;
            }
        }else{
            
        }
            
            
        
    }
}

?>