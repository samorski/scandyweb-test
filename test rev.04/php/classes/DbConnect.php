<?php 
    class DbConnect{
        

        
        public function connect(){
            
            $conn = new PDO("mysql:host=localhost;dbname=scandiweb-test-db;","root","");
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

            return $conn;
        }

        
    }

?>