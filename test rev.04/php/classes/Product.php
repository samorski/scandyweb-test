<?php 

abstract class Product{

    public $sku;
    public $name;
    public $price;
    public $productType;

    public function __construct($sku,$name,$price,$productType)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->productType = $productType;
        
    }

    public function connect(){
            
        $conn = new PDO("mysql:host=localhost;dbname=scandiweb-test-db;","root","");
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        return $conn;
    }
    

   

   

    abstract function setProduct();


    abstract function validateProduct();


   

}

?>