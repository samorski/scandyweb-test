<?php
    require 'DbConnect.php';

    class DatabaseQuery extends DbConnect {

        public function getAllProducts(){
            $sql = "SELECT * FROM `products`   ORDER BY `products`.`id`  DESC";
            $query = $this->connect()->query($sql);
            $row = $query->fetchAll();

            return $row;
        
        }

        public function deleteProductById($id){
            $this->id = $id;

            $sql = "DELETE FROM `products` WHERE `id` IN( {$this->id})";
            $query = $this->connect()->prepare($sql);
            $result = $query->execute();

            //return $result;
            header("location: ../index.php");
        }
        
    }
?>