<?php

    require 'php/classes/DatabaseQuery.php';

    $db = new DatabaseQuery();
    

?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Product List</title>
</head>

<body>
    <div class="container-fluid p-3">
        <form id="delete-form" action="php/deleteProduct.php" method="POST">
            <div class="row mt-5 border-bottom-dark">
                <div class="col-md-6">
                    <h1>Product List</h1>
                </div>
                <div class="col-md-6 ">
                    <div class="float-right mr-3">
                        <a class="btn btn-outline-dark btn-shadow mr-2 mr-5" href="addproduct.php">ADD</a>
                        <button id="delete-product-btn" class="btn btn-outline-dark btn-shadow ">MASS DELETE</button>
                    </div>

                </div>
            </div>
            <div class="border mb-4"></div>
            <div class="row pl-4">
                <?php foreach ($db->getAllProducts() as $key => $content) { ?>
                    
                        <div class="col-md-3  mb-3">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <div>
                                    <input class="delete-checkbox" type="checkbox" name="delete[]" id="<?php echo "{$content["id"]}" ?>" value="<?php echo "{$content["id"]}" ?>">
                                </div>
                                <h6 class="card-title"><?php echo "{$content["SKU"]}" ?></h6>
                                <h5 class="card-subtitle mb-2 text-muted"><?php echo "{$content["name"]}" ?></h5>
                                <p class="card-text"><?php echo "{$content["price"]}" ?> $.</p>
                                <p class="card-text"> 
                                <?php
                                    if($content['product_type'] == 'Furniture'){

                                        echo "Dimension: {$content["height"]} x {$content["width"]} x {$content["length"]} CM." ;
                                    }elseif ($content['product_type'] == 'DVD') {
                                        echo "Size: {$content["size"]} MB." ;
                                    }elseif ($content['product_type'] == 'Book') {
                                        echo "Size: {$content["weight"]} KG." ;
                                    }
                                
                                ?> </p>
                                
                            </div>
                            </div>
                        </div>
                <?php } ?>
            </div>
        </form>
    </div>
    </div>

    <script src="js/jquery-3.6.0.js"></script>
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
    -->
</body>

</html>