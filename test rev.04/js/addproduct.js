$(document).ready(function(){
   
     // form handling SART
    let typeSwitch = $('#productType');
    typeSwitch.on('change', function(e){
        let value = typeSwitch.val();
        let DVD = $('.DVD');
        let DVDi = $('.DVDi');
        let book = $('.book');
        let booki = $('.booki');
        let height = $('.height');
        let heighti = $('.heighti');
        let width = $('.width');
        let widthi = $('.widthi');
        let length = $('.length');
        let lengthi = $('.lengthi')

        if (value == 'DVD') {
            DVD.css('display','inline-block');
            DVDi.css('display','unset');
            book.css('display','none');
            booki.css('display','none');
            width.css('display','none');
            widthi.css('display','none');
            length.css('display','none');
            lengthi.css('display','none');
            height.css('display','none');
            heighti.css('display','none');
        }else if(value == 'Book'){
            book.css('display','inline-block');
            booki.css('display','unset');
            DVD.css('display','none');
            DVDi.css('display','none');
            width.css('display','none');
            widthi.css('display','none');
            length.css('display','none');
            lengthi.css('display','none');
            height.css('display','none');
            heighti.css('display','none');
        }else if(value == 'Furniture'){
            width.css('display','inline-block');
            widthi.css('display','unset');
            height.css('display','inline-block');
            heighti.css('display','unset');
            length.css('display','inline-block');
            lengthi.css('display','unset');
            book.css('display','none');
            booki.css('display','none');
            DVD.css('display','none');
            DVDi.css('display','none');
        };
    });
    // form handling END
    // validation request START
    let productForm = $('#product_form');
     
    let lengthDesc = $('#lengthDesc');
    let widthDesc = $('#widthDesc');
    let heightDesc = $('#heightDesc');
    let weightDesc = $('#weightDesc');
    let sizeDesc = $('#sizeDesc');
    let productTypeDesc = $('#productTypeDesc');
    let priceDesc =$('#priceDesc');
    let nameDesc = $('#nameDesc');
    let skuDesc = $('#skuDesc');

    productForm.on('submit',function(e){
        let sku = $('#sku').val();
        let name = $('#name').val();
        let price = $('#price').val();
        let productType = $('#productType').val();
        let size = $('#size').val();
        let weight = $('#weight').val();
        let height = $('#height').val();
        let width = $('#width').val();
        let length = $('#length').val();
       
         
        if(sku == ''){
            e.preventDefault();
            skuDesc.html(`<p class="text-danger">* SKU field is required</p>`);
        }else{
            skuDesc.text(`Please add the SKU (stok-keeping unit) for the product, this should be unique combination of alphanumeric characters.`)
        }
        if(name == ''){
            e.preventDefault();
            nameDesc.html(`<p class="text-danger">* Name field is required</p>`);
        }
        else{
            nameDesc.text(` Please add the product name.`);
        }
        if(price == ''){
            e.preventDefault();
            priceDesc.html(`<p class="text-danger">* Price field is required</p>`);
        }else{
            priceDesc.text(`Please add the product price.`);
        }
        if(productType == '' || productType == null){
            e.preventDefault();
            productTypeDesc.html(`<p class="text-danger">* Product type field is required</p>`);
        }
        else{
            productTypeDesc.html(` Please add the product type.`);
        }
        if(productType == 'DVD' && size == ''){
            e.preventDefault();
            sizeDesc.html(`<p class="text-danger">* Size field is required</p>`);
        }else {
            sizeDesc.html(`Please add the product size.`);
        }
        if(productType == 'Book' && weight == ''){
            e.preventDefault();
            weightDesc.html(`<p class="text-danger">* Weight field is required</p>`);
        }else{
            weightDesc.html(`Please add the product weight.`);
        }
        if(productType == 'Furniture' && height == ''){
            e.preventDefault();  
            heightDesc.html(`<p class="text-danger">* Height field is required</p>`);
         }else{
            heightDesc.html(`Please add the product height.`);
        }
        if(productType == 'Furniture' && width ==''){
            e.preventDefault();
            widthDesc.html(`<p class="text-danger">* Width field is required</p>`);
        }else{
            widthDesc.html(`Please add the product width.`);
        }
        if(productType == 'Furniture' && length ==''){
            e.preventDefault();
            lengthDesc.html(`<p class="text-danger">* Length field is required</p>`)
        }else{
            lengthDesc.html(`Please add the product lenght.`);
        }
         
    });
    // validation request END
});